package maria.suarez.anchundia.pm.facci.cruduniversidad

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var Siglas: EditText? = null
    private var Nombres: EditText? = null
    private var Carreras: EditText? = null
    private var Categoria: EditText? = null
    private var Ciudad: EditText? = null
    private var id: EditText? = null
    private var guardar: Button? = null
    private var consultar: Button? = null
    private var modificar: Button? = null
    private var eliminarT: Button? = null
    private var Datos: TextView? = null
    private var dataBase: DataBase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Siglas = findViewById(R.id.Siglas) as EditText
        Nombres = findViewById(R.id.Nombre) as EditText
        Carreras = findViewById(R.id.Carreras) as EditText
        Categoria = findViewById(R.id.Categoria) as EditText
        Ciudad = findViewById(R.id.Ciudad) as EditText
        id = findViewById(R.id.Id) as EditText
        guardar = findViewById(R.id.Guardar) as Button
        consultar = findViewById(R.id.consultar) as Button
        modificar = findViewById(R.id.modificar) as Button
        eliminarT = findViewById(R.id.EliminarT) as Button

        guardar!!.setOnClickListener(this)
        consultar!!.setOnClickListener(this)
        modificar!!.setOnClickListener(this)
        eliminarT!!.setOnClickListener(this)
        Datos = findViewById(R.id.Datos) as TextView

        dataBase = DataBase(this)
    }
    override fun onClick(v: View) {

        when (v.id) {
            R.id.Guardar ->

                if (Siglas!!.text.toString().isEmpty()) {

                } else if (Siglas!!.text.toString().isEmpty()) {

                } else if (Nombres!!.text.toString().isEmpty()) {

                } else if (Carreras!!.text.toString().isEmpty()) {

                } else if (Categoria!!.text.toString().isEmpty()) {

                } else if (Ciudad!!.text.toString().isEmpty()) {

                } else {
                    dataBase!!.Insertar(
                        Siglas!!.text.toString(), Nombres!!.text.toString(),
                        Carreras!!.text.toString(), Categoria!!.text.toString(), Ciudad!!.text.toString()
                    )
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show()
                    id!!.setText("")
                    Siglas!!.setText("")
                    Nombres!!.setText("")
                    Carreras!!.setText("")
                    Categoria!!.setText("")
                    Ciudad!!.setText("")
                    Datos!!.text = ""
                }
            R.id.consultar -> {
                Datos!!.setText(dataBase!!.consultarTodos())
                id!!.setText("")
                Siglas!!.setText("")
                Nombres!!.setText("")
                Carreras!!.setText("")
                Categoria!!.setText("")
                Ciudad!!.setText("")
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show()
            }
            R.id.modificar -> if (Siglas!!.text.toString().isEmpty()) {

            } else if (Siglas!!.text.toString().isEmpty()) {

            } else if (Nombres!!.text.toString().isEmpty()) {

            } else if (Carreras!!.text.toString().isEmpty()) {

            } else if (Categoria!!.text.toString().isEmpty()) {

            } else if (Ciudad!!.text.toString().isEmpty()) {

            } else if (id!!.text.toString().isEmpty()) {

            } else {
                dataBase!!.Modificar(
                    id!!.text.toString(), Siglas!!.text.toString(), Nombres!!.text.toString(),
                    Carreras!!.text.toString(), Categoria!!.text.toString(), Ciudad!!.text.toString()
                )
                Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show()
                id!!.setText("")
                Siglas!!.setText("")
                Nombres!!.setText("")
                Carreras!!.setText("")
                Categoria!!.setText("")
                Ciudad!!.setText("")
                Datos!!.text = ""
            }
            R.id.EliminarT -> {
                dataBase!!.EliminarTodo()
                Datos!!.text = ""
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }
}
